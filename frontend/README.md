# Frontend

Run GraphQL server on localhost then install and run the frontend:

```
$ npm install
$ npm run build
$ npm start
```

or you can also run development mode with live reloading:

```
$ npm run dev
```

### Hints

- self-made application structure (something between simple framework and utility libraries) proven on real large business projects
- SPA architecture with base support of responsivity
- server side prerendering
- robust TypeScript typing, newly created typed GrapQL queries and mutations for this challenge (all my projects I am working on use REST API)
- ready for large code base and pleasant maintability and refactoring
- UI models (classes) are separated from React templates, most of logic depends only on plain TS
- most of HTML, CSS (Sass) created from scratch, no complex UI libraries used, only tooling for modals and tooltips, 
- BEM naming methododology and separated Sass files to better maintain large style code base
- graphic design is only schematic for this challenge, I'm more fullstack programmer, coder and UX designer than graphic designer, no optimized pictures, icons, etc.