import * as React from "react";
import { BrowserRouter, StaticRouter, Route, Switch, withRouter, RouteComponentProps } from "react-router-dom";

import * as state from "../fw/lib/state";
import * as common from "../fw/lib/common";
import * as navigation from "../fw/lib/navigation";
import * as routing from "../fw/lib/routing";
import AppContext, { StateContext } from "./context";

import Page404 from "./modules/pages/page404/Page404";
import InformationDialog from "./modules/shared/dialogs/InformationDialog";
import YesNoDialog from "./modules/shared/dialogs/YesNoDialog";
import PokemonDetailDialog from "./modules/shared/pokemons/PokemonDetailDialog";

// Komponenta pro odchycení a registraci history objektu pro práci s HTML5 history API
const HistoryRegistration = withRouter(class extends React.Component<RouteComponentProps> {
	constructor(props: RouteComponentProps) {
		super(props);
	}

	componentDidMount() {
		navigation.registerHistory(this.props.history);
	}

	render() {
		return null;
	}
});

// Komponenta pro "odskrolování nahoru" při aktivaci stránky v react routeru
const ScrollToTop = withRouter(class extends React.Component<RouteComponentProps> {
	componentDidUpdate(prevProps: RouteComponentProps) {
		if (this.props.history.location.pathname !== prevProps.location.pathname) {
			window.scrollTo(0, 0);
		}
	}
	render() {
		return this.props.children;
	}
});

// Komponenta pro načtení dat modulu
const LoadDataTriggerComponent = withRouter(class extends React.Component<RouteComponentProps & { routes: routing.Route[] }> {
	componentDidMount = async () => {
		await routing.loadData(window.location.pathname, this.props.routes, true);
	}

	componentDidUpdate = async (prevProps: RouteComponentProps) => {
		if (this.props.history.location.pathname !== prevProps.location.pathname) {
			await routing.loadData(window.location.pathname, this.props.routes, false);
		}
	}

	render = () => null;
});

/**
 * Abstrakce routeru pro clienta a server
 */
function Router(props: any) {
	return common.serverExecution()
		? <StaticRouter location={props.url}>{props.children}</StaticRouter>
		: <BrowserRouter>{props.children}</BrowserRouter>;
}

interface AppProps {
	url?: string;
	context: StateContext;
	routes: routing.Route[];
}

function AppWithStateContext(props: AppProps) {
	const stateContext = state.useStateContext();

	return (
		<Router url={props.url}>
			<>
				<InformationDialog />
				<YesNoDialog />
				<PokemonDetailDialog />
				<HistoryRegistration />
				<LoadDataTriggerComponent routes={props.routes} />
				<ScrollToTop>
					<Switch>
						{props.routes.map(i => <Route
							key={i.route}
							exact
							path={i.route}
							component={i.component}
							render={i.render}
						/>)}
						{props.routes.map(i => i.aliases?.map(a => <Route
							key={i.route}
							exact
							path={a}
							component={i.component}
							render={i.render}
						/>))}

						{/* Fallback pro neexistující stránky */}
						<Route component={Page404} />
					</Switch>
				</ScrollToTop>
			</>
		</Router>
	);
}

const AppWithStateContextBound = state.bindContainers(
	AppWithStateContext,
	c => ({
		getStateContainers: () => {
			return [
				c.dialogs.stateContainer
			];
		}
	})
);

export default function App(props: AppProps) {
	return (
		<AppContext.Provider value={props.context}>
			<AppWithStateContextBound {...props} />
		</AppContext.Provider>
	);
}
