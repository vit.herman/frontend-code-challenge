import moment from "moment";
import "moment/locale/cs";
import * as formating from "../fw/lib/formatting";

export default function () {
	formating.init();
	moment.locale("cs");
}