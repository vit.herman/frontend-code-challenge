/*
 * Main Layout Page
 */

import React from "react";
import { Helmet } from "react-helmet";
import * as state from "../../../fw/lib/state";

export interface PageProps {
	title: string;
	children: React.ReactNode;
}

export default function Page(props: PageProps) {
	const stateContext = state.useStateContext();
	const { dialogs } = stateContext;

	return (
		<>
			<Helmet>
				<title>{props.title}</title>
			</Helmet>

			<div className={"page" + (dialogs.isSomeDialogOpen() ? " page--modal-open" : "")}>
				{props.children}
			</div>
		</>
	);
}