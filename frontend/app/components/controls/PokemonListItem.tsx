/**
 * Pokemon list item component to show in list page
 */

import React from "react";
import * as state from "../../../fw/lib/state";

interface Pokemon {
	id: string;
	name: string;
	types: string[];
	isFavorite: boolean;
	image: string;
}

export interface PokemonListItemProps {
	pokemon: Pokemon;
	onChange: () => void;
}

export default function PokemonListItem(props: PokemonListItemProps) {
	const context = state.useStateContext();

	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className="pokemon-list-item row">
			<div className="col-md-4 align-self-center text-center text-md-left mb-4 mb-md-0">
				<img className="pokemon-list-item__image" src={props.pokemon.image} />
			</div>
			<div className="col-md-4 align-self-center text-center text-md-left">
				<strong>{props.pokemon.name}</strong><br />
				{props.pokemon.types.join(", ")}
			</div>
			<div className="col-md-4 align-self-center text-center text-md-right">
				<img className="pokemon-list-item__eye mr-2" src="/images/icon-eye.png" onClick={async e => {
					e.preventDefault();
					await context.pokemons.showPokemonDialog(props.pokemon.id, onChange);
				}} />
				<img
					className="pokemon-list-item__heart"
					src={"/images/" + (props.pokemon.isFavorite ? "fullheart.png" : "heart.png")}
					onClick={async (e) => {
						e.preventDefault();
						await context.pokemons.togglefavorite(props.pokemon);
						onChange();
					}}
				/>
			</div>
		</div>
	);
}