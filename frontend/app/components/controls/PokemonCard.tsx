/**
 * Pokemon card component to show in list page
 */

import React from "react";
import { Tooltip } from "react-tippy";
import * as state from "../../../fw/lib/state";

interface Pokemon {
	id: string;
	name: string;
	isFavorite: boolean;
	types?: string[];
	image: string;
}

export interface PokemonCardProps {
	className?: string;
	pokemon: Pokemon;
	onChange: () => void;
}

export default function PokemonCard(props: PokemonCardProps) {
	const context = state.useStateContext();

	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className={"pokemon-card " + (props.className ?? "")}>
			<img className="pokemon-card__eye" src="/images/icon-eye.png" onClick={async e => {
				e.preventDefault();
				await context.pokemons.showPokemonDialog(props.pokemon.id, onChange);
			}} />
			<div className="d-flex align-items-center justify-content-center h-100">
				<img className="pokemon-card__image" src={props.pokemon.image} />
			</div>
			<div className="d-flex justify-content-between align-items-center p-2 bg-gray">
				<div className="text-nowrap">
					<strong>{props.pokemon.name}</strong><br />
					{props.pokemon.types?.join(", ")}
				</div>
				<div>
					<Tooltip
						arrow
						position="bottom-start"
						title={props.pokemon.isFavorite ? "Remove from bookmarks" : "Add to bookmarks"}
						trigger="mouseenter"
					>
						<img
							className="pokemon-card__heart"
							src={"/images/" + (props.pokemon.isFavorite ? "fullheart.png" : "heart.png")}
							onClick={async (e) => {
								e.preventDefault();
								await context.pokemons.togglefavorite(props.pokemon);
								onChange();
							}}
						/>
					</Tooltip>
				</div>
			</div>
		</div>
	);
}