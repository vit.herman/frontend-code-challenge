/**
 * Pokemon component to show detail
 */

import React from "react";
import PokemonCard from "./PokemonCard";
import * as state from "../../../fw/lib/state";

interface NumberRange {
	minimum: string;
	maximum: string;
}

interface PokemonSimple {
	id: string;
	name: string;
	isFavorite: boolean;
	image: string;
}

interface Pokemon extends PokemonSimple {
	types: string[];
	maxCP: number;
	maxHP: number;
	weight: NumberRange;
	height: NumberRange;
	evolutions: PokemonSimple[];
	image: string;
	sound: string;
}

export interface PokemonCardProps {
	pokemon: Pokemon;
	showEvolutions?: boolean;
	onChange?: () => void;
}

export default function PokemonDetail(props: PokemonCardProps) {
	const context = state.useStateContext();

	function onChange() {
		if (props.onChange) {
			props.onChange();
		}
	}

	return (
		<div className="pokemon-detail">
			<div className="text-center p-3 position-relative mb-3">
				<img className="pokemon-detail__play"
					src="/images/icon-play.png"
					onClick={_ => context.pokemons.playPokemon(props.pokemon)} />

				<img className="pokemon-detail__image" src={props.pokemon.image} />
			</div>
			<div className="bg-gray">
				<div className="d-flex justify-content-between align-items-start mb-3 p-3">
					<div className="text-nowrap">
						<strong>{props.pokemon.name}</strong><br />
						{props.pokemon.types.join(", ")}
					</div>
					<div>
						<img
							className="pokemon-detail__heart"
							src={"/images/" + (props.pokemon.isFavorite ? "fullheart.png" : "heart.png")}
							onClick={async (e) => {
								e.preventDefault();
								await context.pokemons.togglefavorite(props.pokemon);
								onChange();
							}}
						/>
					</div>
				</div>
				<div className="d-flex align-items-center">
					<div className="pokemon-detail__progress-line --cp"></div>
					<div className="pokemon-detail__progress-value ml-3">CP: {props.pokemon.maxCP}</div>
				</div>
				<div className="d-flex align-items-center mb-3">
					<div className="pokemon-detail__progress-line --hp"></div>
					<div className="pokemon-detail__progress-value ml-3">HP: {props.pokemon.maxHP}</div>
				</div>
				<div className={"d-flex justify-content-between " + (props.showEvolutions !== false ? "mb-3" : "")}>
					<div className="pokemon-detail__value-box --right-border">
						<strong>Weight</strong><br />
						<small>{props.pokemon.weight.minimum} &ndash; {props.pokemon.weight.maximum}</small>
					</div>
					<div className="pokemon-detail__value-box">
						<strong>Height</strong><br />
						<small>{props.pokemon.height.minimum} &ndash; {props.pokemon.height.maximum}</small>
					</div>
				</div>
			</div>
			{props.showEvolutions !== false && props.pokemon.evolutions.length > 0 &&
				<div className="pokemon-detail__evolutions">
					<div className="font-weight-bold">Evolutions</div>

					<div className="d-flex">
						{props.pokemon.evolutions.map(evolution =>
							<PokemonCard pokemon={evolution} key={evolution.id}
								onChange={onChange} className="mr-2" />
						)}
					</div>
				</div>
			}
		</div>
	);
}