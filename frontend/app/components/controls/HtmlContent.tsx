
import React from "react";
import htmlParse, { HTMLReactParserOptions, domToReact } from "html-react-parser";

import Link from "../../../fw/components/controls/Link";

export interface HtmlContentProps {
	content?: string;
}

const parseOptions: HTMLReactParserOptions = {
	replace: (node: any) => {
		if (node.name === "a") {
			const { class: className, style, href, ...restAttribs } = node.attribs;
			return <Link className={className ?? ""} href={href}>{domToReact(node.children, parseOptions)}</Link>;
		}
	}
};

export default function HtmlContent(props: HtmlContentProps) {
	return (
		<>
			{props.content ? htmlParse(props.content ?? "", parseOptions) : null}
		</>
	);
}