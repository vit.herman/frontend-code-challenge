/**
 * Information dialog
 */
import * as React from "react";
import * as state from "../../../../fw/lib/state";

import Dialog from "./Dialog";
import EnterPressArea from "../../../../fw/components/controls/EnterPressArea";
import HtmlContent from "../../../components/controls/HtmlContent";

function InformationDialog() {
	const { standardDialogs } = state.useStateContext();
	const dialog = standardDialogs.informationDialog;

	return (
		<Dialog dialog={dialog} limitedWidth>
			<EnterPressArea onEnter={dialog.close}>
				<div className="text-center">
					<HtmlContent content={dialog.getCustomField("message")} />
				</div>
				<div className="mt-4 text-center">
					<button onClick={e => standardDialogs.informationDialog.close()} className="pt-2 pb-2">
						<strong>{dialog.getCustomField("buttonOkTitle")}</strong>
					</button>
				</div>
			</EnterPressArea>
		</Dialog >
	);
}

export default state.bindContainers(InformationDialog,
	context => context.standardDialogs.informationDialog
);