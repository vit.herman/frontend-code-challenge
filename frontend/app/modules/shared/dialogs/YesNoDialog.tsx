/**
 * Yes/No dialog
 */
import * as React from "react";
import * as state from "../../../../fw/lib/state";

import Dialog from "./Dialog";
import EnterPressArea from "../../../../fw/components/controls/EnterPressArea";

function InformationDialog() {
	const { standardDialogs } = state.useStateContext();
	const dialog = standardDialogs.yesNoDialog;

	return (
		<Dialog dialog={dialog} limitedWidth>
			<EnterPressArea onEnter={dialog.close}>
				<div className="text-center">
					{dialog.getCustomField("message")}
				</div>
				<div className="mt-30 text-center">
					<button className="mr-10" onClick={e => dialog.close("yes")}>
						<strong>{dialog.getCustomField("buttonYesTitle")}</strong>
					</button>
					<button onClick={e => dialog.close("no")}>
						<strong>{dialog.getCustomField("buttonNoTitle")}</strong>
					</button>
				</div>
			</EnterPressArea>
		</Dialog >
	);
}

export default state.bindContainers(InformationDialog,
	c => c.standardDialogs.yesNoDialog
);