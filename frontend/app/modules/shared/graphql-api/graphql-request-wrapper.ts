/**
 * GraphQL Reguest wrapper for notification of errors 
 */

import * as graphqlApi from "./graphql-api";
import { ExecutionResult } from "graphql";
import * as notification from "../../../../fw/lib/notification";

const wrapper: graphqlApi.SdkFunctionWrapper = async (action, operationName) => {
	try {
		return await action();
	} catch (e) {
		if (e.response && e.response.errors) {
			const message = (e.response as ExecutionResult).errors?.map(e => e.message).join(" ");
			if (message && message.trim().length > 0) {
				notification.errorMessage(message);
			}
		} else {
			const message = "Network connection to server failed. Don't forget to run GraphQL server ;-)";
			notification.errorMessage(message);
		}
		throw e;
	}
};

export default wrapper;
