/**
 * Shared pokemon's model
 */

import * as context from "../../../context";
import * as notification from "../../../../fw/lib/notification";
import * as common from "../../../../fw/lib/common";
import * as dialogs from "../dialogs/dialogs";
import * as grapqlApi from "../graphql-api/graphql-api";

type PokemonDetail = common.PropType<grapqlApi.LoadPokemonByIdQuery, "pokemonById">;

interface DetailDialogFields {
	pokemon: PokemonDetail;
	onChange?: () => void;
}

export class Model {

	detailDialog: dialogs.Dialog<void, DetailDialogFields>;

	constructor(private context: context.StateContext) {
		this.detailDialog = new dialogs.Dialog<void, DetailDialogFields>({
			defaultCustomFields: {
				pokemon: undefined
			}
		}, context);
	}

	togglefavorite = async (pokemon: { id: string, isFavorite: boolean, name: string }) => {
		if (pokemon.isFavorite) {
			await this.context.api.UnFavoritePokemon({ id: pokemon.id });
			notification.successMessage(`${pokemon.name} was removed from favorites`);
		} else {
			await this.context.api.FavoritePokemon({ id: pokemon.id });
			notification.successMessage(`${pokemon.name} was added to favorites`);
		}
	}

	playPokemon = async (pokemon: { sound: string }) => {
		const sound = new Audio(pokemon.sound);
		await sound.play();
	}

	showPokemonDialog = async (pokemonId: string, onChange?: () => void) => {
		const pokemon = await this.context.api.LoadPokemonById({ id: pokemonId });
		await this.detailDialog.setCustomField("pokemon", pokemon.pokemonById);
		await this.detailDialog.setCustomField("onChange", onChange);
		await this.detailDialog.open();
	}

	onDialogPokemonChange = async () => {
		const pokemonOld = this.detailDialog.getCustomField("pokemon");
		const onChange = this.detailDialog.getCustomField("onChange");
		if (pokemonOld) {
			const pokemon = await this.context.api.LoadPokemonById({ id: pokemonOld.id });
			await this.detailDialog.setCustomField("pokemon", pokemon.pokemonById);
			if (onChange) {
				onChange();
			}
		}
	}
}