/**
 * Dialog that shows details about a pokemon
 */
import * as React from "react";
import * as state from "../../../../fw/lib/state";

import Dialog from "../dialogs/Dialog";
import EnterPressArea from "../../../../fw/components/controls/EnterPressArea";
import PokemonDetail from "../../../components/controls/PokemonDetail";

function PokemonDetailDialog() {
	const { pokemons } = state.useStateContext();
	const dialog = pokemons.detailDialog;
	const pokemon = dialog.getCustomField("pokemon");

	return (
		<Dialog dialog={dialog}>
			<EnterPressArea onEnter={dialog.close}>
				{pokemon &&
					<PokemonDetail
						pokemon={pokemon}
						showEvolutions={false}
						onChange={pokemons.onDialogPokemonChange}
					/>
				}
			</EnterPressArea>
		</Dialog >
	);
}

export default state.bindContainers(PokemonDetailDialog,
	c => c.pokemons.detailDialog
);