/**
 * Pokemons list page
 */
import React from "react";
import { Tooltip } from "react-tippy";
import * as state from "../../../../fw/lib/state";
import Page from "../../../components/templates/Page";
import PokemonCard from "../../../components/controls/PokemonCard";
import PokemonListItem from "../../../components/controls/PokemonListItem";
import Link from "../../../../fw/components/controls/Link";
import Paginator from "../../../../fw/components/controls/Paginator";

function PokemonsListPage() {
	const { pokemonsList: model } = state.useStateContext();
	const pokemons = model.getPokemons();

	return (
		<Page title="Pokemons">
			<div className="container pokemons-list-page">
				<div className="mb-4">
					<div className="mb-2">
						<button
							className={"pokemons-list-page__button-tab " + (!model.getFilterFavorites() ? "--active" : "")}
							onClick={_ => model.setFilterFavorites(false)}
						>
							All
						</button>
						<button
							className={"pokemons-list-page__button-tab " + (model.getFilterFavorites() ? "--active" : "")}
							onClick={_ => model.setFilterFavorites(true)}
						>
							Favorites
						</button>
					</div>
					<div className="row">
						<div className="col-md-8">
							<input
								className="pokemons-list-page__search mb-2"
								value={model.getSearch()}
								onChange={e => model.setSearch(e.currentTarget.value)}
								placeholder="search"
								autoFocus
							/>
						</div>
						<div className="col-md-4">
							<div className="d-flex align-items-center">
								<select
									value={model.getFilterType()}
									onChange={e => model.setFilterType(e.currentTarget.value)}
									placeholder="type"
									className={"w-100 mb-2 mr-gutter " + (model.getFilterType().length === 0 ? "color-placeholder" : "")}
								>
									<option value="">-- All types --</option>
									{model.getPokemonTypes().map(pokemonType =>
										<option key={pokemonType} value={pokemonType}>{pokemonType}</option>
									)}
								</select>
								<Tooltip
									arrow
									position="bottom-start"
									title="view as list"
									trigger="mouseenter"
								>
									<img className="pokemons-list-page__icon mb-2 mr-gutter" src="/images/icon-list.png" onClick={model.setViewList} />
								</Tooltip>
								<Tooltip
									arrow
									position="bottom-start"
									title="view as grid"
									trigger="mouseenter"
								>
									<img className="pokemons-list-page__icon mb-2" src="/images/icon-grid.png" onClick={model.setViewGrid} />
								</Tooltip>
							</div>
						</div>
					</div>
				</div>
				{pokemons.count > 0 &&
					<div>
						<div className="text-center text-md-right mb-4">
							<Paginator
								className="d-block mb-2"
								limit={pokemons.limit}
								offset={pokemons.offset}
								count={pokemons.count}
								onOffsetChanged={model.reloadPage}
							/>
						</div>

						{model.getView() === "grid" &&
							<div className="row">
								{pokemons.edges.map(pokemon =>
									<div key={pokemon.id} className="col-md-4 col-lg-2 mb-gutter">
										<Link href={"/" + pokemon.name} className="--no-style">
											<PokemonCard pokemon={pokemon} onChange={model.reloadPage} />
										</Link>
									</div>
								)}
							</div>
						}
						{model.getView() === "list" &&
							<ul className="pokemons-list-page__list">
								{pokemons.edges.map(pokemon =>
									<li>
										<Link key={pokemon.id} href={"/" + pokemon.name} className="d-block --no-style">
											<PokemonListItem pokemon={pokemon} onChange={model.reloadPage} />
										</Link>
									</li>
								)}
							</ul>
						}
					</div>
				}
				{pokemons.count == 0 &&
					<p>
						No pokemons found.
					</p>
				}
			</div>
		</Page >
	);
}

export default state.bindContainers(
	PokemonsListPage,
	context => context.pokemonsList
);