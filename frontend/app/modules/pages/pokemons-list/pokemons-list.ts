/**
 * Model for pokemons list page
 */

import * as state from "../../../../fw/lib/state";
import * as common from "../../../../fw/lib/common";
import * as context from "../../../context";
import * as grapqlApi from "../../shared/graphql-api/graphql-api";

type View = "list" | "grid";
type Pokemons = common.PropType<grapqlApi.LoadPokemonsListQuery, "pokemons">;

/**
 * Module state definition
 */
interface State {
	pokemons: Pokemons;
	pokemonTypes: string[];
	search: string;
	filterType: string;
	filterFavorites: boolean;
	view: View;
}

/**
 * Page model
 */
export class Model implements state.StateModel {
	stateContainer: state.StateContainer<State>;

	constructor(private context: context.StateContext) {
		this.stateContainer = new state.StateContainer<State>({
			pokemons: {
				count: 0,
				edges: [],
				limit: 0,
				offset: 0
			},
			search: "",
			pokemonTypes: [],
			filterType: "",
			filterFavorites: false,
			view: "grid",
		}, context);
	}

	/**
	* Returns collection of state containers
	*/
	getStateContainers = () => [
		this.stateContainer
	]

	/**
	 * Loads pokemons
	 */
	loadPokemons = async () => {
		await this.reloadPage(0);
	}

	/**
	 * Reloads current page data
	 */
	reloadPage = async (offset: number | undefined = undefined) => {
		const result = await this.context.api.LoadPokemonsList({
			filter: {
				offset: offset ?? this.getPokemons().offset,
				search: this.getSearch(),
				filter: {
					type: this.getFilterType(),
					isFavorite: this.getFilterFavorites(),
				}
			}
		});

		if (result.pokemons.count > 0 && result.pokemons.edges.length === 0) {
			// skip to last nonempty page
			await this.reloadPage(Math.floor(result.pokemons.count / result.pokemons.limit));
		} else {
			await this.stateContainer.merge(_ => ({
				pokemons: result.pokemons
			}));
		}
	}

	/**
	 * Load pokemons debounced
	 */
	loadPokemonsDebounced = common.debounce(this.loadPokemons, 250);

	/**
	 * Loads pokemon types
	 */
	loadPokemonTypes = async () => {
		const result = await this.context.api.LoadPokemonTypes();

		await this.stateContainer.merge(_ => ({
			pokemonTypes: result.pokemonTypes
		}));
	}

	/**
	 * Returns loaded pokemons
	 */
	getPokemonTypes = () => {
		return this.stateContainer.get().pokemonTypes;
	}

	/**
	 * Returns loaded pokemons
	 */
	getPokemons = () => {
		return this.stateContainer.get().pokemons;
	}

	/**
	 * Set search phrase 
	 */
	setSearch = async (search: string) => {
		await this.stateContainer.merge(_ => ({
			search
		}));
		this.loadPokemonsDebounced();
	}

	/**
	 * Get search phrase 
	 */
	getSearch = () => {
		return this.stateContainer.get().search;
	}

	/**
	 * Set filter by pokemon type 
	 */
	setFilterType = async (filterType: string) => {
		await this.stateContainer.merge(_ => ({
			filterType
		}));
		await this.loadPokemons();
	}

	/**
	 * Get filter by pokemon type
	 */
	getFilterType = () => {
		return this.stateContainer.get().filterType;
	}

	/**
	 * Set filter for favorites
	 */
	setFilterFavorites = async (filterFavorites: boolean) => {
		await this.stateContainer.merge(_ => ({
			filterFavorites: filterFavorites
		}));
		await this.loadPokemons();
	}

	/**
	 * Get filter for favorites
	 */
	getFilterFavorites = () => {
		return this.stateContainer.get().filterFavorites;
	}

	/**
	 * Set grid view
	 */
	setViewGrid = async () => {
		await this.stateContainer.merge(_ => ({
			view: "grid"
		}));
	}

	/**
	 * Set list view
	 */
	setViewList = async () => {
		await this.stateContainer.merge(_ => ({
			view: "list"
		}));
	}

	/**
	 * Get current view type 
	 */
	getView = () => {
		return this.stateContainer.get().view;
	}

	showPokemonDialog = () => {

	}

	/**
	 * Loads page data
	 */
	loadData = async () => {
		await Promise.all([
			this.loadPokemonTypes(),
			this.reloadPage()
		]);
	}
}