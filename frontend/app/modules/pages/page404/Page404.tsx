/**
 * 404 Page
 */
import * as React from "react";
import Page from "../../../components/templates/Page";

export default class Page404 extends React.Component {
	render() {
		return (
			<Page title="Page not exists!">
				<main role="main">
					<div className="container mb-40">
						<div className="row">
							<div className="col col-xl-5">
								<h2>Page does not found!</h2>
								<p>
									It's possible the page has been removed, renamed or is temporary unavailable.
									Please check your bookmarks or page URL.
								</p>
							</div>
						</div>
					</div>
				</main>
			</Page >
		);
	}
}