/**
 * Model for pokemon's detail page
 */

import * as state from "../../../../fw/lib/state";
import * as common from "../../../../fw/lib/common";
import * as context from "../../../context";
import * as grapqlApi from "../../shared/graphql-api/graphql-api";

type Pokemon = common.PropType<grapqlApi.LoadPokemonByNameQuery, "pokemonByName">;

/**
 * Module state definition
 */
interface State {
	pokemon?: Pokemon;
}

/**
 * Page model
 */
export class Model implements state.StateModel {
	stateContainer: state.StateContainer<State>;

	constructor(private context: context.StateContext) {
		this.stateContainer = new state.StateContainer<State>({}, context);
	}

	/**
	* Returns collection of state containers
	*/
	getStateContainers = () => [
		this.stateContainer
	]

	/**
	 * Loads pokemons
	 */
	loadPokemon = async (pokemonName: string) => {
		const result = await this.context.api.LoadPokemonByName({
			name: pokemonName
		});

		await this.stateContainer.merge(_ => ({
			pokemon: result.pokemonByName
		}));
	}

	/**
	 * Reloads current pokemon
	 */
	reloadPokemon = async () => {
		const pokemon = this.getPokemon();
		if (!pokemon) {
			return;
		}
		await this.loadPokemon(pokemon.name);
	}

	/**
	 * Returns loaded pokemon
	 */
	getPokemon = () => {
		return this.stateContainer.get().pokemon;
	}

	/**
	 * Loads page data
	 */
	loadData = async (pokemonName: string) => {
		await this.loadPokemon(pokemonName);
	}
}