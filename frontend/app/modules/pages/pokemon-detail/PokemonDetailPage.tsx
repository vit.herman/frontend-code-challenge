/**
 * Pokemon's detail page
 */
import React from "react";
import * as state from "../../../../fw/lib/state";
import Page from "../../../components/templates/Page";
import PokemonDetail from "../../../components/controls/PokemonDetail";

function PokemonPage() {
	const { pokemonDetail: model } = state.useStateContext();
	const pokemon = model.getPokemon();

	return (
		<Page title={pokemon?.name ?? "Pokemon"}>
			<div className="pokemon-detail-page">
				{pokemon &&
					<PokemonDetail pokemon={pokemon} onChange={model.reloadPokemon} />
				}
			</div>
		</Page >
	);
}

export default state.bindContainers(
	PokemonPage,
	context => context.pokemonDetail
);