/**
 * Application context
 */

import * as React from "react";
import * as state from "../fw/lib/state";

// GraphQL API
import { GraphQLClient } from "graphql-request";
import * as graphqlApi from "./modules/shared/graphql-api/graphql-api";
import graphqlRequestWrapper from "./modules/shared/graphql-api/graphql-request-wrapper";

// core modules
import * as dialogs from "./modules/shared/dialogs/dialogs";
import * as standardDialogs from "./modules/shared/dialogs/standard-dialogs";
import * as pokemons from "./modules/shared/pokemons/pokemons";

// page modules
import * as pokemonsList from "./modules/pages/pokemons-list/pokemons-list";
import * as pokemonDetail from "./modules/pages/pokemon-detail/pokemon-detail";

export class StateContext {

	// GraphQL API module
	api = graphqlApi.getSdk(
		new GraphQLClient("http://localhost:4000/graphql"),
		graphqlRequestWrapper
	);

	// base modules
	state = new state.StateContainerContext();
	dialogs = new dialogs.Model(this);
	standardDialogs = new standardDialogs.Model(this);

	// shared modules
	pokemons = new pokemons.Model(this);

	// page models
	pokemonsList = new pokemonsList.Model(this);
	pokemonDetail = new pokemonDetail.Model(this);
}

const AppContext = React.createContext<StateContext>(undefined as any);
export default AppContext;
