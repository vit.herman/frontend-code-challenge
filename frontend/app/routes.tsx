import * as routing from "../fw/lib/routing";
import { StateContext } from "./context";

import PokemonsListPage from "./modules/pages/pokemons-list/PokemonsListPage";
import PokemonPage from "./modules/pages/pokemon-detail/PokemonDetailPage";

export default function createRoutes(context: StateContext): routing.Route[] {
	return [
		{
			route: "/",
			component: PokemonsListPage,
			loadData: context.pokemonsList.loadData,
			loadSsr: true
		},
		{
			route: "/:name",
			component: PokemonPage,
			loadData: match => context.pokemonDetail.loadData((match as any).params.name),
			loadSsr: true
		}
	];
}