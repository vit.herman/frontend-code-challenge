process.env["NODE_ENV"] = process.env["NODE_ENV"] || "development";

import React from "react";
import express from "express";
import ReactDOMServer from "react-dom/server";
import fs from "fs";
import util from "util";

import * as routing from "../fw/lib/routing";
import * as cookies from "../fw/lib/cookies";
import appinit from "../app/init";
import createRoutes from "../app/routes";
import { StateContext } from "../app/context";

import App from "../app/App";

appinit();
const server = express();
server.use(express.static("../client"));

// handle SPA request
server.get("/*", async function (req, res) {
	cookies.init(req.headers.cookie);
	const stateContext = new StateContext();
	const routes = createRoutes(stateContext);

	// load data for content generating
	try {
		await routing.loadData(req.path, routes);
	} catch (error) {
		console.log("error fetching data: ");
		console.log(error);
	}

	// rendering of content from App component
	const stringContent = ReactDOMServer.renderToString(
		React.createElement(
			App, {
			url: req.url,
			context: stateContext,
			routes: routes
		})
	);

	// render page from template
	const readFile = util.promisify(fs.readFile);

	let response = await readFile("../../index.html", { encoding: "utf8" });
	response = response
		.replace("./styles/app.scss", `/styles.css`)
		.replace("// {{header-script}}", stateContext.state.renderHookedSsrState())
		.replace("window.developmentMode = true;", "")
		.replace("<!-- {{content}} -->", stringContent)
		.replace("./client/client.ts", `/scripts.js`);

	res.send(response);
});

// run server itself
const port = process.env.PORT || 8080;
server.listen(port, () => console.log(`Web Pokemons (${process.env.NODE_ENV}) listening on port ${port}!`));