import UniversalCookie, { CookieSetOptions } from "universal-cookie";

let cookies: UniversalCookie = new UniversalCookie();

export function init(serverCookies: string | undefined) {
	cookies = new UniversalCookie(serverCookies);
}

export function get(name: string) {
	return cookies.get<string | undefined>(name);
}

export function set(name: string, value: string, options: CookieSetOptions) {
	cookies.set(name, value, options);
}

export function remove(name: string) {
	cookies.remove(name);
}