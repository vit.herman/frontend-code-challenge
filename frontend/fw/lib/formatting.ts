/**
 * Parsování a formátování.
 */

import numeral from "numeral";
import "numeral/locales";
import moment from "moment";

export function init() {
	numeral.locale("cs");
}

/**
 * Parsuje řetězec jako celé číslo. Vrací: 
 * 		- celé číslo, pokud je řetězec platné celé číslo
 * 		- null, pokud je vstupní řetězec prázdný nebo null
 * 		- NaN, pokud je vstupní řetězec v neplatném formátu
 */
export function parseWholeNumber(value: string | undefined) {
	if (value === undefined || value.trim() == "") {
		return undefined;
	}
	const num = numeral(value).value();
	if (num === null) {
		return NaN;
	} else {
		return num;
	}
}

/**
 *  Parsuje řetězec jako datum.
 */
export function parseDate(value: string) {
	const normalizedValue = value.replace(/ /g, "");
	let mdate = moment(normalizedValue, "D.M.YYYY", true);
	if (!mdate.isValid()) {
		mdate = moment(normalizedValue, moment.ISO_8601);
	}
	return mdate.isValid() ? mdate.toDate() : undefined;
}

/**
 * Zformátuje číslo jako celé číslo (zaokrouhlí případnou desetinou část).
 */
export function formatWholeNumber(value: number | undefined) {
	if (value === undefined) {
		return "";
	} else {
		return numeral(value).format();
	}
}

/**
 * Zformátuje číslo jako měnu.
 */
export function formatCurrency(value: number | undefined) {
	if (value === undefined) {
		return "";
	} else {
		return numeral(value).format() + " Kč";
	}
}

/**
 * Naformátuje hodnotu jako procento.
 */
export function formatPercent(value: number | undefined) {
	if (value === undefined) {
		return "";
	} else {
		return numeral(value * 100).format() + " %";
	}
}

/**
 * Naformátuje hodnotu jako datum.
 */
export function formatDate(value: Date | undefined, options?: { format?: "long" | "iso" | "strip" | "strip-long" }) {
	if (value === undefined) {
		return "";
	} else {
		const now = new Date();
		let format = "D. M. YYYY";

		if (options?.format === "iso") {
			format = "YYYY-MM-DD";
		}
		else if (options?.format === "long") {
			format = "D. MMMM. YYYY";
		} else if (options?.format === "strip") {
			if (now.getFullYear() === value.getFullYear()) {
				format = "D. M.";
			}
		} else if (options?.format === "strip-long") {
			format = now.getFullYear() === value.getFullYear() ? "D. MMMM" : "D. MMMM YYYY";
		}

		return moment(value).format(format);
	}
}

/**
 * Naformátuje hodnotu jako datum s časem nebo pouze čas, jedná-li se o dnešek
 */
export function formatDateTime(value: Date | undefined, options?: { format?: "strip" }) {
	if (value === undefined) {
		return "";
	} else {
		const now = new Date();
		let format = "D. M. YYYY H:mm";

		if (options?.format === "strip") {
			if (moment(value).isSame(now, "day")) {
				format = "H:mm";
			} else if (moment(value).isSame(now, "year")) {
				format = "D. M.";
			}
		}

		return moment(value).format(format);
	}
}

export function pluralize(num: number, one: string, twofour: string, zeroOrMore: string) {
	if (num == 1) {
		return one;
	} else if (num >= 2 && num <= 4) {
		return twofour;
	} else if (num == 0 || num >= 5) {
		return zeroOrMore;
	}
}